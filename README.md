# bashrc

My bash settings.

To checkout, clone this repository to `~/.profile.d`, then get the
submoudles: `cd ~/.profile.d;git submodule init;git submodule update`.

To enable, add the following code to your `~/.profile`

```shell
# source general profile information.
source ~/.profile

#
# Source profile extensions for certain packages.
#
if test -d $HOME/.bashrc.d ; then
    for s in $HOME/.bashrc.d/*.sh ; do
        test -r $s -a ! -k $s && . $s
    done
    unset s
fi
```
