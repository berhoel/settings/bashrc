#! /bin/bash

# Time-stamp: <2022-07-10 19:38:49 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Environment settings for GCC.

# Author: Berthold Höllmann <berhoel@gmail.com>

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa gcc.sh"
# End:
