#! /bin/bash

# Time-stamp: <2024-05-05 21:21:57 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for shell history.

# Author: Berthold Höllmann <berhoel@gmail.com>

# lt. c't 2008/4, S189 um bei mehreren geöffneten Terminals die
# history von allen zu erhalten.
shopt -s histappend

PROMPT_COMMAND="history -a; history -c; history -r;echo -ne \"\033]0;${USER}@${HOSTNAME}: ${PWD}\007\"; $PROMPT_COMMAND"

export HISTTIMEFORMAT="%F %T: "

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa history.sh"
# End:
