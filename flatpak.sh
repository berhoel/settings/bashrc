#! /bin/bash

# Time-stamp: <2023-06-18 11:53:38 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings to access flatpak installed packages.

# Author: Berthold Höllmann <berhoel@gmail.com>

# Interactive shell
if [ -x "$(command -v flatpak)" ]; then
    upd_flatpak() {
        if tty -s; then
            # Update flatpack packages. Run only one at a time.
            flock -n -E 0 "${BASH_SOURCE[0]}" flatpak update --noninteractive
        fi
    }

    mapfile -t __flat_commands < <(flatpak list --columns=name,application --user --app)
    for __l in "${__flat_commands[@]}";do
        __alias_name="$(cut -f 1 <<<"$__l" | sed 's/ /_/g')"
        __alias_command="$(cut -f 2 <<<"$__l")"
        eval "alias ${__alias_name}=\"flatpak run ${__alias_command}\""
    done

    unset __flat_commands __l __alias_name __alias_command
fi

# installed (2023-06-18)
# Name                                            Application ID
# HeiseArchiv                                     de.heise.app.HeiseArchiv
# Flowblade                                       io.github.jliljebl.Flowblade
# Podman Desktop                                  io.podman_desktop.PodmanDesktop
# Eclipse IDE for Java Developers                 org.eclipse.Java
# FreeCAD                                         org.freecadweb.FreeCAD
# Freeplane                                       org.freeplane.App
# Gaphor                                          org.gaphor.Gaphor
# GeoGebra                                        org.geogebra.GeoGebra

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa flatpak.sh"
# End:
