#! /bin/bash

# Time-stamp: <2022-07-10 19:45:33 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for bash prompt theme.

# Author: Berthold Höllmann <berhoel@gmail.com>

THEME="$HOME/.bashrc.d/themes/agnoster-bash/agnoster.bash"
export THEME
if [[ -f "${THEME}" ]]; then
    DEFAULT_USER="$(whoami)"
    export DEFAULT_USER
    # shellcheck source=/dev/null
    source "${THEME}"
fi

set-window-title() {
    echo -en "\033]0;$USER:$(pwd | sed -e "s;^$HOME;~;")\a"
}

if [[ "$PROMPT_COMMAND" ]]; then
    export PROMPT_COMMAND="$PROMPT_COMMAND;set-window-title"
else
    export PROMPT_COMMAND=set-window-title
fi

enable-save-history() {
    history -a
    history -c
    history -r
}

if [[ "$PROMPT_COMMAND" ]]; then
    PROMPT_COMMAND="$PROMPT_COMMAND;enable-save-history"
else
    PROMPT_COMMAND=enable-save-history
fi
export PROMPT_COMMAND

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa theme.sh"
# End:
