#! /bin/bash

# Time-stamp: <2024-01-09 22:26:00 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Fortune on login.

# Author: Berthold Höllmann <berhoel@gmail.com>

if [ -n "$PS1" ] # have prompt?
then   # interactive
    # Guten Tag
    echo " "
    fortune | awk '{print "               " $0}' -
    echo " "
    if command -v please 1>/dev/null 2>&1; then
        flock "${BASH_SOURCE[0]}" please
        eval "$(please --show-completion bash)"
    fi
fi


# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa fortune.sh"
# End:

