# -*- mode: sh; sh-shell: bash; -*-

# Time-stamp: <2024-05-05 21:31:54 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for the arx package

# Author: Berthold Höllmann <berhoel@gmail.com>

# some more ls aliases
# if [[ $(type -t _ls) == function ]]
# then
#     alias ls=_ls
# else
#     alias ls='ls ${LS_OPTIONS}'
# fi
LSD_OPTIONS="-N --color=always"
alias ls="lsd ${LSD_OPTIONS}" 

alias ll='ls -alF'
alias la='ls -A'

alias dir='ls -l'
alias l='ls -l'
# https://opensource.com/article/19/7/bash-aliases?utm_medium=email&utm_source=topic+optin&utm_campaign=awareness&utm_content=20190810+prog+nl&mkt_tok=eyJpIjoiTmpGallqVmxabVJtWlRRMiIsInQiOiJES2k3aVVJQ0pKSHhidWN6ZnBsa3g0NDlcL0VuR1JoZXZjN2xnaUM4VHN4QnhKMzFjb2ZFVmlaMGhpMXpqWXdld1FiSVUwbUlWZ2VNdmtSZkJod1lOWlArXC92d0tFNGVkQmFlN1YxdXZjaGtoWWdLTEhxSWYyc1JISldpRWRCcFNTIn0%3D
# Sort by file size
alias lt='ls --human-readable --sizesort -1 -S --classify'

alias unix2dos='recode lat1:ibmpc'
alias dos2unix='recode ibmpc:lat1'

alias h='history'

alias xdvi='LANG=C xdvi'

alias cal='\cal -m'

alias dia='env LC_NUMERIC=C dia'

# View only mounted drives
alias mnt="mount | awk -F' ' '{ printf \"%s\t%s\n\",\$1,\$3; }' | column -t | egrep ^/dev/ | sort"

# Create a Python virtual environment
alias ve='python3 -m venv ./venv'
alias va='source ./venv/bin/activate'

# Fix for calling gv
alias gv='LC_ALL=C gv'

# alias for emacs
alias em="setsid emacs"

# terminals
alias rxvt="urxvt --font xft:JetBrainsMono:medium:size=12 -geometry 120x50"
alias gt="setsid gnome-terminal"

# use full terminal width for diff
alias diff='diff -W "$(tput cols)"'

alias more=less

ec() {
    emacsclient "$@" || (emacs "$@" & )
}

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xas bash aliases.sh"
# End:
