#! /bin/bash

# Time-stamp: <2022-07-10 19:38:48 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : direnv - unclutter your .profile

# Author: Berthold Höllmann <berhoel@gmail.com>

eval "$(direnv hook bash)"

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa direnv.sh"
# End:
