#! /bin/bash

# Time-stamp: <2022-07-10 19:46:54 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : fasd - quick access to files and directories

# Author: Berthold Höllmann <berhoel@gmail.com>

fasd_cache="$HOME/.fasd-init-bash"
if [ "$(command -v fasd)" -nt "$fasd_cache" ] || [ ! -s "$fasd_cache" ]; then
	fasd --init posix-alias bash-hook bash-ccomp bash-ccomp-install >|"$fasd_cache"
fi

# shellcheck source=/dev/null
source "$fasd_cache"
unset fasd_cache

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa fasd.sh"
# End:
