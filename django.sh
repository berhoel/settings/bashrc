#! /bin/bash

# Time-stamp: <2022-07-10 19:38:48 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for Django projectsq

# Author: Berthold Höllmann <berhoel@gmail.com>

# # Django
# export DJANGO_SETTINGS_MODULE=myproject.settings
# export PYTHONPATH=$HOME/work/Django:$PYTHONPATH

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa django.sh"
# End:
