#! /bin/bash

# Time-stamp: <2024-01-09 22:26:37 hoel>

# Copyright © 2021 by Berthold Höllmann

# Task  : Settings for working with python.

# Author: Berthold Höllmann <berhoel@gmail.com>

if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

if command -v pipx 1>/dev/null 2>&1; then
    eval "$(register-python-argcomplete pipx)"
fi

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa python.sh"
# End:
