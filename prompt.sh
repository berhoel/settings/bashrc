#! /bin/bash

# Time-stamp: <2022-07-10 19:40:37 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for commandline prompt.

# Author: Berthold Höllmann <berhoel@gmail.com>

#export PS1="%d %D.%w, %T: %B%n@%m:%b %c02 [$shlvl]%#"
#export PS1='$PS1\[\033]0;\u@\h:\w\007\]'

function proml() {
	case $TERM in
	xterm*)
		local TITLEBAR='\[\033]0;\u@\h:\w\007\]'
		;;
	*)
		local TITLEBAR=''
		;;
	esac

	# PS1="${TITLEBAR}\
	# [\$(date +%H:%M)]\
	# [\u@\h:\w]\
	# \$ "
	PS1="${TITLEBAR}$PS1"
	PS2='> '
	PS4='+ '
}

proml
unset proml

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa prompt.sh"
# End:
