#! /bin/bash

# Time-stamp: <2022-07-10 19:51:10 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for applications in the home directory.

# Author: Berthold Höllmann <berhoel@gmail.com>

# set -vx

PY_UTOK=$(dirname "${BASH_SOURCE[0]}")/pyutok/utok.py

utok() {
    python "${PY_UTOK}" "$@"
}

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa 00_home.sh"
# End:
