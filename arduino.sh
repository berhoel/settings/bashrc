#! /bin/bash

# Time-stamp: <2022-07-10 19:42:11 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Setup for Arduino, for using Arduini.mk.

# Author: Berthold Höllmann <berhoel@gmail.com>

# export ARDUINO_DIR=/home/hoel/bin/arduino-1.8.12
# export ARDMK_DIR=/home/hoel/Arduino/Arduino-Makefile
# export AVR_TOOLS_DIR=/usr/include

# export ESP_HOME=/home/hoel/Arduino/esp-open-sdk
# export SMING_HOME=/home/hoel/Arduino/Sming/Sming

if [ -d /home/hoel/Arduino/arduino-cli/bin ] ; then
    PATH=$(utok "$PATH" /home/hoel/Arduino/arduino-cli/bin)
    export PATH
fi

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa arduino.sh"
# End:
