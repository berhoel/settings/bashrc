#! /bin/bash

# Time-stamp: <2024-05-05 21:53:51 hoel>

# Copyright © 2024 by kumkeo GmbH

# Task  : NPM update function.

# Author: Berthold Höllmann <berhoel@gmail.com>

# Find outdated npm packages. Run only one at a time.
upd_npm () {
    # Interactive shell
    flock -n -E 0 "${BASH_SOURCE[0]}" ncu -g
}

# Local Variables:
# compile-command: "shellcheck -xas bash npm.sh"
# End:
