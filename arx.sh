#! /bin/bash

# Time-stamp: <2022-07-10 19:48:08 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for the arx package

# Author: Berthold Höllmann <berhoel@gmail.com>

# This procedure was created by the /ARX installation procedure.
# Include this file in your "/etc/profile" startup command
# (or "/etc/default/login" if you prefer this).
# for public access to the /ARX software.
# Do not forget the "." in front of the command (ex.: ". /etc/arx.conf")
# For selected users access include it in the users ".profile" or ".cshrc".
ARX=/usr/local/arx
MANPATH=$(utok "${MANPATH}" "${ARX}"/man)

export ARX MANPATH

# MacroCalc environment
# shellcheck source=/dev/null
test -f /etc/arx.conf.sh && . /etc/arx.conf.sh

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa arx.sh"
# End:
